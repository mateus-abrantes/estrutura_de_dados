TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    mergesort.h \
    gerador_grafo_completo.h \
    gerador_grafo_grid.h \
    gerador_numero_aleatorio.h
