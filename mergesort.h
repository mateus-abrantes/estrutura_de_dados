#ifndef MERGESORT_H
#define MERGESORT_H
#define MAX1 1000
#include<bits/stdc++.h>

int AUX1[MAX1], AUX2[MAX1];
void merge(int vetor[MAX1], int p, int q, int r) {  //  O(n)
    int n_AUX1 = q - p + 1;
    int n_AUX2 = r - q;
    for (int i=0; i<n_AUX1; i++) AUX1[i] = vetor[p+i];
    for (int i=0; i<n_AUX2; i++) AUX2[i] = vetor[q+i+1];
    AUX1[n_AUX1] = INT_MAX;
    AUX2[n_AUX2] = INT_MAX;
    int i=0;
    int j=0;
    for (int k=p; k<=r; k++){
        if (AUX1[i]<=AUX2[j]){
            vetor[k] = AUX1[i];
            i++;
        } else {
            vetor[k] = AUX2[j];
            j++;
        }
    }
}

// complexidade = O(nlogn)
void mergeSort(int vetor[MAX1], int comeco, int fim){
    if (comeco < fim) {
        int meio = (fim+comeco)/2;

        mergeSort(vetor, comeco, meio);
        mergeSort(vetor, meio+1, fim);
        merge(vetor, comeco, meio, fim); // O(n)
    }
}


#endif // MERGESORT_H
