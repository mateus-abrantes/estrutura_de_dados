#ifndef GERADOR_GRAFO_GRID_H
#define GERADOR_GRAFO_GRID_H
#include <fstream>
#include<iostream>
#include<ctime>
#include<string.h>

#include "gerador_numero_aleatorio.h"
void GeradorGrafoGrid(int n){
    ofstream outputFilename ("grid/"+to_string(n)+"grid.in");
    srand((unsigned int)time(0));
    outputFilename << n <<endl;
    for(int i =1; i<=n;i++){
        if (outputFilename.is_open()) {
            if((i <= n/2)){
                if(i == n/2){
                    outputFilename <<i<<" "<<i+(n/2)<<" "<<numeroaleatorio(1,2*n)<<endl;
                }else{
                    outputFilename <<i<<" "<<i+1<<" "<<numeroaleatorio(1,2*n)<<endl;
                    outputFilename <<i<<" "<<i+(n/2)<<" "<<numeroaleatorio(1,2*n)<<endl;
                }
            }
            else{
                if(i<n){
                    outputFilename <<i<<" "<<i+1<<" "<<numeroaleatorio(1,2*n)<<endl;
                }
            }
        }else{
            cout<<"Nao foi possivel abrir o arquivo de saida\n";
            exit(1);

        }
    }
    outputFilename.close();
 }
#endif // GERADOR_GRAFO_GRID_H
